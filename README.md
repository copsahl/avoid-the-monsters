# avoid-the-monsters

A program I wrote for my assembly class that was supposed to resemble Pac-Man. With a little more time I could make the ghosts AI a lot better than it is at the moment. 

A lot of the base code for this program was provided by my instructor. So big thanks to him for getting me started!