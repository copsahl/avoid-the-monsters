%include "/usr/local/share/csc314/asm_io.inc"

; the file that stores the initial state
%define BOARD_FILE 'board2.txt'

; how to represent everything
%define WALL_CHAR '#'
%define PLAYER_CHAR 'O'
%define MONSTER_CHAR 'G'
%define NOMNOM_CHAR '*'
%define EMPTY_CHAR ' '
%define TICK 100000 ; 1/10 of a second

; the size of the game screen in characters
%define HEIGHT 20
%define WIDTH 19

; the player starting position.
; top left is considered (0,0)
%define STARTX 9
%define STARTY 9

; blinky starting position
%define BSTARTX 4
%define BSTARTY 3

; inky starting position
%define ISTARTX 1
%define ISTARTY 17

; pinky starting position
%define PSTARTX 14
%define PSTARTY 3

; clyde starting position
%define CSTARTX 17
%define CSTARTY 17

; these keys do things
%define EXITCHAR 'x'
%define UPCHAR 'w'
%define LEFTCHAR 'a'
%define DOWNCHAR 's'
%define RIGHTCHAR 'd'


segment .data

        ; used to fopen() the board file defined above
        board_file                      db BOARD_FILE,0

        ; used to change the terminal mode
        mode_r                          db "r",0
        raw_mode_on_cmd         db "stty raw -echo",0
        raw_mode_off_cmd        db "stty -raw echo",0

        ; called by system() to clear/refresh the screen
        clear_screen_cmd        db "clear",0

        ; things the program will print
        help_str                        db 13,10,"Controls: ", \
                                                        UPCHAR,"=UP / ", \
                                                        LEFTCHAR,"=LEFT / ", \
                                                        DOWNCHAR,"=DOWN / ", \
                                                        RIGHTCHAR,"=RIGHT / ", \
                                                        EXITCHAR,"=EXIT", \
                                                        13,10,10,0

        you_died                        db      "You ran into a monster! Game Over!",10,0
        you_win                         db      "YOU ARE THE WINNER! YOU COLLECTED ALL THE NOM NOMS!",10,0
        nomnom_counter          dd      0
        nomnom_max                      dd      148
        nomnom_fmt                      db      "Nom Noms: %d", 10, 13, 0

segment .bss

        ; this array stores the current rendered gameboard (HxW)
        board   resb    (HEIGHT * WIDTH)

        ; these variables store the current player position
        xpos    resd    1
        ypos    resd    1

        ; blinky
        bxpos   resd    1
        bypos   resd    1

        ; inky
        ixpos   resd    1
        iypos   resd    1

        ; pinky
        pxpos   resd    1
        pypos   resd    1

        ; clyde
        cxpos   resd    1
        cypos   resd    1


segment .text

        global  asm_main
        global  raw_mode_on
        global  raw_mode_off
        global  init_board
        global  render

        extern  system
        extern  putchar
        extern  getchar
        extern  printf
        extern  fopen
        extern  fread
        extern  fgetc
        extern  fclose
        extern  usleep
        extern  fcntl

asm_main:
        enter   0,0
        pusha
        ;***************CODE STARTS HERE***************************

        ; put the terminal in raw mode so the game works nicely
        call    raw_mode_on

        ; read the game board file into the global variable
        call    init_board

        ; set the player at the proper start position
        mov             DWORD [xpos], STARTX
        mov             DWORD [ypos], STARTY

        ; set monsters at proper start position
        mov             DWORD [bxpos], BSTARTX
        mov             DWORD [bypos], BSTARTY

        mov             DWORD [ixpos], ISTARTX
        mov             DWORD [iypos], ISTARTY

        mov             DWORD [pxpos], PSTARTX
        mov             DWORD [pypos], PSTARTY

        mov             DWORD [cxpos], CSTARTX
        mov             DWORD [cypos], CSTARTY


        ; the game happens in this loop
        ; the steps are...
        ;   1. render (draw) the current board
        ;   2. get a character from the user
        ;       3. store current xpos,ypos in esi,edi
        ;       4. update xpos,ypos based on character from user
        ;       5. check what's in the buffer (board) at new xpos,ypos
        ;       6. if it's a wall, reset xpos,ypos to saved esi,edi
        ;       7. otherwise, just continue! (xpos,ypos are ok)
        game_loop:

                ; check to see if player has collected all the nom noms
                cmp             DWORD [nomnom_counter], 13
                jge             winner

                ; draw the game board
                call    render

                ; get an action from the user
                call    getchar

                ; store the current position
                ; we will test if the new position is legal
                ; if not, we will restore these
                mov             esi, [xpos]
                mov             edi, [ypos]

                ; choose what to do
                cmp             eax, EXITCHAR
                je              game_loop_end
                cmp             eax, UPCHAR
                je              move_up
                cmp             eax, LEFTCHAR
                je              move_left
                cmp             eax, DOWNCHAR
                je              move_down
                cmp             eax, RIGHTCHAR
                je              move_right
                jmp             input_end                       ; or just do nothing

                ; move the player according to the input character
                move_up:
                        dec             DWORD [ypos]
                        jmp             input_end
                move_left:
                        dec             DWORD [xpos]
                        jmp             input_end
                move_down:
                        inc             DWORD [ypos]
                        jmp             input_end
                move_right:
                        inc             DWORD [xpos]
                input_end:

                ; (W * y) + x = pos

                ; Make ghosts move
                ; blinky
                mov             ebx, DWORD [bxpos]
                mov             ecx, DWORD [bypos]
                mv_blinky:
                cmp             DWORD [bypos], edi      ; Compare the ghosts y position to the players y position
                jg              b_dec_y
                cmp             DWORD [bypos], edi
                jl              b_inc_y

                cmp             DWORD [bxpos], esi
                jg              b_dec_x

                inc             DWORD [bxpos]
                jmp             test_b
                b_dec_x:
                dec             DWORD [bxpos]
                jmp             test_b
                b_dec_y:
                dec             DWORD [bypos]
                jmp             test_b
                b_inc_y:
                inc             DWORD [bypos]

                test_b:
                mov     eax, WIDTH
        mul     DWORD [bypos]
        add     eax, [bxpos]
        lea     eax, [board + eax]
        cmp     BYTE [eax], WALL_CHAR
        jne     valid_move_blinky
            ; oops, that was an invalid move, reset
            mov     DWORD [bxpos], ebx
            mov     DWORD [bypos], ecx
                valid_move_blinky:


                mov             ebx, DWORD [pxpos]
                mov             ecx, DWORD [pypos]
                mv_pinky:
                cmp             DWORD [pypos], edi      ; Compare the ghosts y position to the players y position
                jg              p_dec_y
                cmp             DWORD [pypos], edi
                jl              p_inc_y

                cmp             DWORD [pxpos], esi
                jg              p_dec_x

                inc             DWORD [pxpos]
                jmp             test_p
                p_dec_x:
                dec             DWORD [pxpos]
                jmp             test_p
                p_dec_y:
                dec             DWORD [pypos]
                jmp             test_p
                p_inc_y:
                inc             DWORD [pypos]

                test_p:
                mov     eax, WIDTH
        mul     DWORD [pypos]
        add     eax, [pxpos]
        lea     eax, [board + eax]
        cmp     BYTE [eax], WALL_CHAR
        jne     valid_move_pinky
            ; oops, that was an invalid move, reset
            mov     DWORD [pxpos], ebx
            mov     DWORD [pypos], ecx
                valid_move_pinky:

                mov             ebx, DWORD [ixpos]
                mov             ecx, DWORD [iypos]
                mv_inky:
                cmp             DWORD [iypos], edi      ; Compare the ghosts y position to the players y position
                jg              i_dec_y
                cmp             DWORD [iypos], edi
                jl              i_inc_y

                cmp             DWORD [ixpos], esi
                jg              i_dec_x

                inc             DWORD [ixpos]
                jmp             test_i
                i_dec_x:
                dec             DWORD [ixpos]
                jmp             test_i
                i_dec_y:
                dec             DWORD [iypos]
                jmp             test_i
                i_inc_y:
                inc             DWORD [iypos]

                test_i:
                mov     eax, WIDTH
        mul     DWORD [iypos]
        add     eax, [ixpos]
        lea     eax, [board + eax]
        cmp     BYTE [eax], WALL_CHAR
        jne     valid_move_inky
            ; oops, that was an invalid move, reset
            mov     DWORD [ixpos], ebx
            mov     DWORD [iypos], ecx
                valid_move_inky:

                mov             ebx, DWORD [cxpos]
                mov             ecx, DWORD [cypos]
                mv_clyde:
                cmp             DWORD [cypos], edi      ; Compare the ghosts y position to the players y position
                jg              c_dec_y
                cmp             DWORD [cypos], edi
                jl              c_inc_y

                cmp             DWORD [cxpos], esi
                jg              c_dec_x

                inc             DWORD [cxpos]
                jmp             test_c
                c_dec_x:
                dec             DWORD [cxpos]
                jmp             test_c
                c_dec_y:
                dec             DWORD [cypos]
                jmp             test_c
                c_inc_y:
                inc             DWORD [cypos]

                test_c:
                mov     eax, WIDTH
        mul     DWORD [cypos]
        add     eax, [cxpos]
        lea     eax, [board + eax]
        cmp     BYTE [eax], WALL_CHAR
        jne     valid_move_clyde
            ; oops, that was an invalid move, reset
            mov     DWORD [cxpos], ebx
            mov     DWORD [cypos], ecx
                valid_move_clyde:


                ; Check collision
                ; blinky
                cmp             esi, DWORD [bxpos]
                jne             pinky_pos
                cmp             edi, DWORD [bypos]
                jne             pinky_pos
                        push    you_died
                        call    printf
                        add             esp, 4
                        jmp             game_loop_end

                pinky_pos:
                cmp             esi, DWORD [pxpos]
                jne             inky_pos
                cmp             edi, DWORD [pypos]
                jne             inky_pos
                        push    you_died
                        call    printf
                        add             esp, 4
                        jmp             game_loop_end

                inky_pos:
                cmp             esi, DWORD [ixpos]
                jne             clyde_pos
                cmp             edi, DWORD [ixpos]
                jne             clyde_pos
                        push    you_died
                        call    printf
                        add             esp, 4
                        jmp             game_loop_end

                clyde_pos:
                cmp             esi, DWORD [cxpos]
                jne             end_pos
                cmp             edi, DWORD [cypos]
                jne             end_pos
                        push    you_died
                        call    printf
                        add             esp, 4
                        jmp             game_loop_end

                end_pos:

                ; compare the current position to the wall character
                mov             eax, WIDTH
                mul             DWORD [ypos]
                add             eax, [xpos]
                lea             eax, [board + eax]
                cmp             BYTE [eax], WALL_CHAR
                jne             valid_move
                        ; oops, that was an invalid move, reset
                        mov             DWORD [xpos], esi
                        mov             DWORD [ypos], edi
                valid_move:

                        cmp     BYTE [eax], NOMNOM_CHAR
                        jne             not_nomnom
                                inc             DWORD [nomnom_counter]
                                mov             BYTE [eax], EMPTY_CHAR
                        not_nomnom:

        jmp             game_loop

        winner:
        call    render
        push    you_win
        call    printf
        add             esp,4
        game_loop_end:

        ; restore old terminal functionality
        call raw_mode_off

        ;***************CODE ENDS HERE*****************************
        popa
        mov             eax, 0
        leave
        ret

; === FUNCTION ===
raw_mode_on:

        push    ebp
        mov             ebp, esp

        push    raw_mode_on_cmd
        call    system
        add             esp, 4

        mov             esp, ebp
        pop             ebp
        ret

; === FUNCTION ===
raw_mode_off:

        push    ebp
        mov             ebp, esp

        push    raw_mode_off_cmd
        call    system
        add             esp, 4

        mov             esp, ebp
        pop             ebp
        ret

; === FUNCTION ===
init_board:

        push    ebp
        mov             ebp, esp

        ; FILE* and loop counter
        ; ebp-4, ebp-8
        sub             esp, 8

        ; open the file
        push    mode_r
        push    board_file
        call    fopen
        add             esp, 8
        mov             DWORD [ebp-4], eax

        ; read the file data into the global buffer
        ; line-by-line so we can ignore the newline characters
        mov             DWORD [ebp-8], 0
        read_loop:
        cmp             DWORD [ebp-8], HEIGHT
        je              read_loop_end

                ; find the offset (WIDTH * counter)
                mov             eax, WIDTH
                mul             DWORD [ebp-8]
                lea             ebx, [board + eax]

                ; read the bytes into the buffer
                push    DWORD [ebp-4]
                push    WIDTH
                push    1
                push    ebx
                call    fread
                add             esp, 16

                ; slurp up the newline
                push    DWORD [ebp-4]
                call    fgetc
                add             esp, 4

        inc             DWORD [ebp-8]
        jmp             read_loop
        read_loop_end:

        ; close the open file handle
        push    DWORD [ebp-4]
        call    fclose
        add             esp, 4

        mov             esp, ebp
        pop             ebp
        ret

; === FUNCTION ===
render:

        push    ebp
        mov             ebp, esp

        ; two ints, for two loop counters
        ; ebp-4, ebp-8
        sub             esp, 8

        ; clear the screen
        push    clear_screen_cmd
        call    system
        add             esp, 4

        ; print the help information
        push    help_str
        call    printf
        add             esp, 4

        ; print the score
        push    DWORD [nomnom_counter]
        push    nomnom_fmt
        call    printf
        add             esp, 8

        ; outside loop by height
        ; i.e. for(c=0; c<height; c++)
        mov             DWORD [ebp-4], 0
        y_loop_start:
        cmp             DWORD [ebp-4], HEIGHT
        je              y_loop_end

                ; inside loop by width
                ; i.e. for(c=0; c<width; c++)
                mov             DWORD [ebp-8], 0
                x_loop_start:
                cmp             DWORD [ebp-8], WIDTH
                je              x_loop_end

                        ; check if (xpos,ypos)=(x,y)
                        mov             eax, [xpos]
                        cmp             eax, DWORD [ebp-8]
                        jne             blinky
                        mov             eax, [ypos]
                        cmp             eax, DWORD [ebp-4]
                        jne             blinky
                                ; if both were equal, print the player
                                push    PLAYER_CHAR
                                call    putchar
                                add             esp, 4
                                jmp             tester

                        blinky:
                        ; check if (bxpos, bypos) = (x,y)
                        mov             eax, [bxpos]
                        cmp             eax, DWORD [ebp - 8]
                        jne             inky
                        mov             eax, [bypos]
                        cmp             eax, DWORD [ebp - 4]
                        jne             inky
                                push    MONSTER_CHAR
                                call    putchar
                                add             esp, 4
                                jmp             tester

                        inky:
                        ; check if (ixpos, iypos) = (x,y)
                        mov             eax, [ixpos]
                        cmp             eax, DWORD [ebp - 8]
                        jne             pinky
                        mov             eax, [iypos]
                        cmp             eax, DWORD [ebp - 4]
                        jne             pinky
                                push    MONSTER_CHAR
                                call    putchar
                                add             esp, 4
                                jmp             tester

                        pinky:
                        ; check if (pxpos, pypos) = (x,y)
                        mov             eax, [pxpos]
                        cmp             eax, DWORD [ebp - 8]
                        jne             clyde
                        mov             eax, [pypos]
                        cmp             eax, DWORD [ebp - 4]
                        jne             clyde
                                push    MONSTER_CHAR
                                call    putchar
                                add             esp, 4
                                jmp             tester

                        clyde:
                        ; check if (cxpos, cypos) = (x,y)
                        mov             eax, [cxpos]
                        cmp             eax, DWORD [ebp - 8]
                        jne             print_board
                        mov             eax, [cypos]
                        cmp             eax, DWORD [ebp - 4]
                        jne             print_board
                                push    MONSTER_CHAR
                                call    putchar
                                add             esp, 4
                                jmp             tester

                        print_board:
                                ; otherwise print whatever's in the buffer
                                mov             eax, [ebp-4]
                                mov             ebx, WIDTH
                                mul             ebx
                                add             eax, [ebp-8]
                                mov             ebx, 0
                                mov             bl, BYTE [board + eax]
                                push    ebx

                        print_end:
                        call    putchar
                        add             esp, 4
                        tester:
                inc             DWORD [ebp-8]
                jmp             x_loop_start
                x_loop_end:

                ; write a carriage return (necessary when in raw mode)
                push    0x0d
                call    putchar
                add             esp, 4

                ; write a newline
                push    0x0a
                call    putchar
                add             esp, 4

        inc             DWORD [ebp-4]
        jmp             y_loop_start
        y_loop_end:

        mov             esp, ebp
        pop             ebp
        ret
